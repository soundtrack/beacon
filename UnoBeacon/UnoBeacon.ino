#include <Wire.h>

#define MASTER
//if master is defined, the selected board will trigger a synchronization pulse signal ~1sec after boot
//else, the board is a slave waiting for sync pulse
//To sync several beacons : one must be a master. Connect all SYNC_PIN together (and GNDs), power up all slaves, the master one at the end.
//LED is on until sync is done.


//PINs definition
#define SYNC_PIN 3
#define LED 4


//Variables
const int threshold=50;
const int average=62;
unsigned long sync_time = 0;  // time reference for synchronization. In microseconds
byte adcVal=0;
byte prevData=0;
byte newData= 0;
int numSamples=0;
int period=0;
int timer=0;
int sum=0;
int frequency;


int changed=0; 

unsigned long nowActivity=0;
unsigned long lastActivity=0;

void synchronize(){
  sync_time = micros();
}

void setup() {

  
  Serial.begin(115200);
  //Wire.begin(); // join i2c bus (address optional for master)
  pinMode(LED, OUTPUT);
  pinMode(7,OUTPUT);

  
  digitalWrite(LED, 1); 
  // time synchronization on boot
#ifdef MASTER
  pinMode(SYNC_PIN, OUTPUT);
  digitalWrite(SYNC_PIN, 1);
  delay(1000);
  digitalWrite(SYNC_PIN, 0);  // start a pulse signal (falling edge)
  synchronize();
  delay(10);
#else
  pinMode(SYNC_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(SYNC_PIN), synchronize, FALLING);
  while (!sync_time==0) {
    //Wait for synchronisation pulse
  }
  detachInterrupt(digitalPinToInterrupt(SYNC_PIN));
#endif
digitalWrite(LED, 0);


  ADCSRA = 0;             // clear ADCSRA register
  ADCSRB = 0;             // clear ADCSRB register
  //ADMUX |= (0 & 0x07);    // set A0 analog input pin
  ADMUX |= (1 << REFS0);  // set reference voltage
  ADMUX |= (1 << ADLAR);  // left align ADC value to 8 bits from ADCH register

  // sampling rate is [ADC clock] / [prescaler] / [conversion clock cycles]
  // for Arduino Uno ADC clock is 16 MHz and a conversion takes 13 clock cycles
  //
  ADCSRA |= (1 << ADPS2) | (1 << ADPS0);    // 32 prescaler for 38.5 KHz
  //ADCSRA |= (1 << ADPS2);                     // 16 prescaler for 76.9 KHz
  //ADCSRA |= (1 << ADPS1) | (1 << ADPS0);    // 8 prescaler for 153.8 KHz

  ADCSRA |= (1 << ADATE); // enable auto trigger
  ADCSRA |= (1 << ADIE);  // enable interrupts when measurement complete
  ADCSRA |= (1 << ADEN);  // enable ADC
  ADCSRA |= (1 << ADSC);  // start ADC measurements
  sei();


  pinMode(13,OUTPUT);
}


ISR(ADC_vect)
{
  adcVal = ADCH;  // read 8 bit value from ADC
  numSamples++;
  //digitalWrite(7, 1);
}



void loop() {

/*if(digitalRead(7)){ 
  digitalWrite(7,0);
}*/

if( !(adcVal==0) and (changed==0) ){

  if( (adcVal > (average + threshold)) || (adcVal < (average - threshold)) ){
    nowActivity=(micros()-sync_time);
    digitalWrite(13, 0);

    delay(100);
    //Serial.println(nowActivity);
    
    changed = 1;
  }

  
  newData=adcVal;
  adcVal=0;

  
}  else if(changed==1){
    //digitalWrite(13, 1);
    digitalWrite(13,1);
    changed=0;
  }


if(numSamples>10){
  //Serial.println(newData);
  numSamples=0;
}


}

